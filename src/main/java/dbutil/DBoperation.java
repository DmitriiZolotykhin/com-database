package dbutil;

import org.apache.log4j.Logger;

import java.sql.*;

public class DBoperation {
    private static final Logger log = Logger.getLogger(DBoperation.class);

    public static boolean createDatabase() {
        Connection connection = DBConnection.getConnection();
        try {
            Statement stmt = connection.createStatement();

            String sql =
                    "CREATE DATABASE bd_for_test  ";
            log.info("database  created.");
            stmt.executeUpdate(sql);

            return true;
        } catch (Exception e) {
        }
        return false;

    }

    public static boolean createTableBookings() {
        Connection connection = DBConnection.getConnection();
        try {
            Statement stmt = connection.createStatement();

            String sql =
                    "CREATE TABLE bookings ( book_ref     varchar(30) primary key, total_amount   int, book_date "
                            + "  date); CREATE TABLE tickets ( book_ref     varchar(30)  REFERENCES bookings(book_ref), " +
                            "ticket_number  varchar(30) primary key, passenger_id int, passenger_name varchar(60), " +
                            "passport_number int, " +
                            "contact_date   date); ";

            stmt.executeUpdate(sql);
            log.info("Table bookings created.");
            return true;
        } catch (Exception e) {
        }
        return false;

    }

    public static boolean createTableTickets() {
        Connection connection = DBConnection.getConnection();
        try {
            Statement stmt = connection.createStatement();
            String sql =
                    " \"CREATE TABLE tickets (\\n\" +\n" +
                            " \" book_ref     varchar(30)  REFERENCES bookings(book_ref),\\n\" +\n" +
                            " \" ticket_number  varchar(30) primary key,\\n\" +\n" +
                            "   \"\\t    passenger_id int,\\n\" +\n" +
                            "   \"\\t    passenger_name varchar(60),\\n\" +\n" +
                            "    \"\\t    passport_number int,\\n\" +\n" +
                            "   \"        contact_date   date\\n\" +\n" +
                            "  \"\\t\\n\" +\n" +
                            "   \");\"; ";
            log.info("Table tickets created.");
            stmt.executeUpdate(sql);
            return true;
        } catch (Exception e) {
        }
        return false;

    }

    public static boolean createTableTicketsFligths() {
        Connection connection = DBConnection.getConnection();
        try {
            Statement stmt = connection.createStatement();

            String sql =
                    " \"CREATE TABLE tickets_fligths (\\n\" +\n" +
                            " \" book_ref     varchar(30)  REFERENCES bookings(book_ref),\\n\" +\n" +
                            " \" ticket_number  varchar(30) primary key,\\n\" +\n" +
                            "   \"\\t    passenger_id int,\\n\" +\n" +
                            "   \"\\t    passenger_name varchar(60),\\n\" +\n" +
                            "    \"\\t    passport_number int,\\n\" +\n" +
                            "   \"        contact_date   date\\n\" +\n" +
                            "  \"\\t\\n\" +\n";
            log.info("Table tickets_fligths created.");
            stmt.executeUpdate(sql);
            return true;
        } catch (Exception e) {
        }
        return false;

    }


    public static boolean insertTableBookings() {
        Connection connection = DBConnection.getConnection();
        try {
            Statement stmt = connection.createStatement();

            String sql =
                    "INSERT into bookings (book_ref,total_amount, book_date )\n" +
                            "VALUES ('ASW999999', 8000, '2019-09-25');\n" +
                            "\n" +
                            "INSERT into bookings (book_ref,total_amount, book_date )\n" +
                            "VALUES ('ASW543203', 10000, '2019-09-25');\n" +
                            "INSERT into bookings (book_ref,total_amount, book_date )\n" +
                            "VALUES ('ABN543293', 34500, '2019-09-11');\n" +
                            "INSERT into bookings (book_ref,total_amount, book_date ) " +
                            "VALUES ('Test88', 300000, '2019-09-25');" +
                            "INSERT into bookings (book_ref,total_amount, book_date )\n" +
                            "    VALUES ('DDS543543', 3000, '2019-09-01');\n" +
                            "\t\tINSERT into bookings (book_ref,total_amount, book_date )\n" +
                            "    VALUES ('ANN543203', 125000, '2018-09-23');\n" +
                            "\t\n" +
                            "\t\tINSERT into bookings (book_ref,total_amount, book_date )\n" +
                            "    VALUES ('QWW541193', 23000, '2018-07-12');\n" +
                            "\t\tINSERT into bookings (book_ref,total_amount, book_date )\n" +
                            "    VALUES ('SWR543883', 455000, '2019-09-02');\n" +
                            "\n" +
                            "INSERT into bookings (book_ref,total_amount, book_date )\n" +
                            "VALUES ('AGR223956', 9000, '2019-09-25');";
            stmt.executeUpdate(sql);
            log.info("Insert into table bookings.");
            return true;
        } catch (Exception e) {
        }
        return false;

    }

    public static boolean insertTableTickets() {
        Connection connection = DBConnection.getConnection();
        try {
            Statement stmt = connection.createStatement();

            String sql =
                    "INSERT into tickets (book_ref,ticket_number, passenger_id,passenger_name,passport_number,contact_date )\n" +
                            "VALUES ('QWW541193', 'AI00003', 7,'Демидов Иван Сергеевич', 455334,'2019-09-20');\n" +
                            "\n" +
                            "INSERT into tickets (book_ref,ticket_number, passenger_id,passenger_name,passport_number,contact_date )\n" +
                            "VALUES ('ANN543203', 'AI00002', 8,'Расторгуева Лилия Николавевна', 455009,'2018-02-03');\n" +
                            "INSERT into tickets (book_ref,ticket_number, passenger_id,passenger_name,passport_number,contact_date )\n" +
                            "VALUES ('DDS543543', 'AI00001', 9,'Чернов Дмитрий Анатольевич', 776334,'2018-07-11');\n" +
                            "INSERT into tickets (book_ref,ticket_number, passenger_id,passenger_name,passport_number,contact_date )\n" +
                            "VALUES ('ABN543293', 'AI00013', 10,'Селиван Петр Иванович', 221334,'2019-02-04');\n" +
                            "INSERT into tickets (book_ref,ticket_number, passenger_id,passenger_name,passport_number,contact_date )\n" +
                            "VALUES ('AGR223956', 'AI00014', 12,'Василисова Елена Петровна', 221334,'2019-01-02');\n" +
                            "\n" +
                            "INSERT into tickets (book_ref,ticket_number, passenger_id,passenger_name,passport_number,contact_date )\n" +
                            "VALUES ('QWW541193', 'AI00004', 7,'Демидов Иван Сергеевич', 455334,'2019-12-20');";
            log.info("Insert into table tickets.");
            stmt.executeUpdate(sql);
            return true;
        } catch (Exception e) {
        }
        return false;

    }


    public static boolean insertTableTicketsFligths() {
        Connection connection = DBConnection.getConnection();
        try {
            Statement stmt = connection.createStatement();
            String sql =
                    "INSERT into tickets_fligths (ticket_number, flight_id,fare_conditions,amount )\n" +
                            "VALUES ( 'AI00003', 999,'Багаж', 4500);\n" +
                            "\n" +
                            "INSERT into tickets_fligths (ticket_number, flight_id,fare_conditions,amount )\n" +
                            "VALUES ( 'AI00004', 909,'Багаж + ручная кладь', 10000);\n" +
                            "\n" +
                            "INSERT into tickets_fligths (ticket_number, flight_id,fare_conditions,amount )\n" +
                            "VALUES ( 'AI00013', 112,'Багаж', 23000);\n" +
                            "\n" +
                            "INSERT into tickets_fligths (ticket_number, flight_id,fare_conditions,amount )\n" +
                            "VALUES ( 'AI00012', 124,'Багаж + ручная кладь', 9000);\n" +
                            "\n" +
                            "INSERT into tickets_fligths (ticket_number, flight_id,fare_conditions,amount )\n" +
                            "VALUES ( 'AI00014', 123,'Багаж', 10000);\n" +
                            "\n" +
                            "\n" +
                            "INSERT into tickets_fligths (ticket_number, flight_id,fare_conditions,amount )\n" +
                            "VALUES ( 'AI00001', 001,'Багаж', 122000);";
            log.info("Insert into table tickets_fligths.");
            stmt.executeUpdate(sql);

            return true;
        } catch (Exception e) {
        }
        return false;

    }

    public static boolean createFunction() {
        Connection connection = DBConnection.getConnection();
        try {
            Statement stmt = connection.createStatement();

            String sql =
                    "\n" +
                            "create or replace function total()\n" +
                            "returns integer as $total$\n" +
                            "declare\n" +
                            "total integer;\n" +
                            "begin\n" +
                            "select sum(total_amount) into total from bookings;\n" +
                            "return total;\n" +
                            "end;\n" +
                            "$total$ language plpgsql;\n;" +
                            "\n";
            stmt.executeUpdate(sql);
            log.info("create function");
            return true;
        } catch (Exception e) {
        }
        return false;

    }

    public static boolean createTrigger() {
        Connection connection = DBConnection.getConnection();
        try {
            Statement stmt = connection.createStatement();
            String sql =

                    "CREATE OR REPLACE FUNCTION bookings () RETURNS trigger AS\n" +
                            "$body$\n" +
                            "BEGIN\n" +
                            "if old.\"total_amount\" =125000  then\n" +
                            "Raise exception 'This record can not be deleted!';\n" +
                            "end if;\n" +
                            "  RETURN old;\n" +
                            "END;\n" +
                            "$body$\n" +
                            "LANGUAGE 'plpgsql' VOLATILE CALLED ON NULL INPUT SECURITY INVOKER;\n" +
                            "\n" +
                            "CREATE TRIGGER total_amount_del BEFORE DELETE \n" +
                            "ON bookings FOR EACH ROW \n" +
                            "EXECUTE PROCEDURE bookings(); ";
            stmt.executeUpdate(sql);
            log.info("create trigger");
            return true;
        } catch (Exception e) {
        }
        return false;

    }

    public static int FindMaxTotalAmount() {
        Connection connection = DBConnection.getConnection();
        int max = 0, x;

        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT  max(total_amount) from bookings");
            while (rs.next()) {
                x = rs.getInt("max");
                rs.getInt("max");
                if (x > max) max = x;
            }
            log.info("return max value from bookings:" + max);
            return max;

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return max;
    }

    public static int FindMinTotalAmount() {
        Connection connection = DBConnection.getConnection();
        int min = 999999999, x;
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM bookings");
            while (rs.next()) {
                x = rs.getInt("total_amount");
                if (x < min) min = x;
            }
            log.info("return min value:" + min);
            return min;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return min;
    }


    public static String useOfLIKE() throws SQLException {
        Connection connection = DBConnection.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM bookings WHERE book_ref LIKE 'AB%'");

        String x = "";
        while (rs.next()) {
            x = rs.getString("book_ref");
            log.info("return  value with coincidence:" + "book_ref = " + x);
        }

        return x;
    }

    public static int fastTotal() throws SQLException {
        Connection connection = DBConnection.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("select total()");

        int x = 0;
        while (rs.next()) {
            x = rs.getInt("total");
            log.info("return fastTotal value:" + x);
        }
        return x;
    }


    public static String bookingTicketJoin() throws SQLException {
        Connection connection = DBConnection.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT bookings.book_ref,tickets.ticket_number,tickets.passenger_name FROM bookings join tickets on bookings.book_ref=tickets.book_ref WHERE bookings.book_ref LIKE 'AB%'");
        String x = "";
        while (rs.next()) {
            x = rs.getString("book_ref");
            log.info("return  value with coincidence for join:" + "book_ref = " + x);
        }

        return x;
    }


    }

